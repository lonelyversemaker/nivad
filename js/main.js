$(function() {

    const headerSliderPaging = $('#header__slider_paging');

    const headerSlider = $('.slider__container_header');


    const blogSlider = $('#slider__container_blog');

    const articleSlider = $('#article__slider');

    headerSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        headerSliderPaging.html(`<span>0${i}</span>/0${slick.slideCount}`);
    });

    blogSlider.slick({
        dots: true,
        appendArrows: $('#project__slider_actions'),
        appendDots: $('#project__slider_pagination'),
        prevArrow: '<span class="slider__arrow prev"></span>',
        nextArrow: '<span class="slider__arrow next"></span>',
        customPaging: function(slick,index) {
            return '<a>' + (index + 1) + '</a>';
        }
    })



    headerSlider.slick({
        appendArrows: $('#header__slider_buttons'),
        prevArrow: '<span class="slider__arrow prev"></span>',
        nextArrow: '<span class="slider__arrow next"></span>'
    })



    articleSlider.slick({
        dots: true,
        appendArrows: $('#article__slider_actions'),
        appendDots: $('#article__slider_pagination'),
        prevArrow: '<span class="slider__arrow prev"></span>',
        nextArrow: '<span class="slider__arrow next"></span>',
        customPaging: function(slick,index) {
            return '<a>' + (index + 1) + '</a>';
        }
    })

    $('.phoneMask').mask('+7 (999) 999-99-99');

    $('.checkbox__custom').click(function(){
		if ($('#agreement').prop('checked')) {
		$(this).siblings('button').prop('disabled', false);
		} else {
			$(this).siblings('button').prop('disabled', true);
		}
    })
    
    $( '.open__nav' ).click(function() {
        $('.mobile__nav_container').css('height', '100vh')
      });

    $('.close__nav').click(function() {
        $('.mobile__nav_container').css('height', '0')
      });

      $('[data-fancybox="gallery"]').fancybox({
        buttons: [
            // "zoom",
            //"share",
            // "slideShow",
            //"fullScreen",
            //"download",
            "thumbs",
            "close"
          ]
    });


    if (window.matchMedia('(max-width: 767px)').matches) {


    } else if ( window.matchMedia('(min-width : 1025px)').matches)  {
        const overflowSlider = $('.slider__container_multiple');

        overflowSlider.on('setPosition', function(){
            const sliderImage = $('.slide__image').outerHeight();
            const sectionHeaderHeight = $('.section__header').outerHeight();
            $('#projects__slider_buttons').css('margin-top', sliderImage - sectionHeaderHeight - 20);
          });

            overflowSlider.slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                appendArrows: $('#projects__slider_buttons'),
                prevArrow: '<span class="slider__arrow prev"></span>',
                nextArrow: '<span class="slider__arrow next"></span>', 		
                responsive: [
                {
                        breakpoint: 1024,
                        settings: {
                            arrows: false,
                        }
                    },
                
                {
                        breakpoint: 767,
                        settings: {
                            // vertical: true,
                            slidesToShow: 1,
                            // swipe: false,
                            // slidesToScroll: 1,
                            // rows: 1, 
                            // slidesPerRow: 1,
                        }
                    }]
            })

            const projectsSlider = $('#slider__container_projects');

            projectsSlider.slick({
                dots: true,
                appendArrows: $('#project__slider_actions'),
                appendDots: $('#project__slider_pagination'),
                prevArrow: '<span class="slider__arrow prev"></span>',
                nextArrow: '<span class="slider__arrow next"></span>',
                customPaging: function(slick,index) {
                    return '<a>' + (index + 1) + '</a>';
                }
            })
              
    } else if ( window.matchMedia('screen and (min-width : 768px) and (max-width : 1024px)').matches ) {
        const projectTabledSlider = $('.slider__container_projects.tablet');

        projectTabledSlider.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            appendArrows: $('#project__slider_actions'),
            appendDots: $('#project__slider_pagination'),
            prevArrow: '<span class="slider__arrow prev"></span>',
            nextArrow: '<span class="slider__arrow next"></span>',
            customPaging: function(slick,index) {
                return '<a>' + (index + 1) + '</a>';
            }
        })

        const teamSliderMobile = $('.slider__container_multiple.mobile');

        teamSliderMobile.slick({
            dots: true,
            appendArrows: $('#project__slider_actions'),
            appendDots: $('#project__slider_pagination'),
            prevArrow: '<span class="slider__arrow prev"></span>',
            nextArrow: '<span class="slider__arrow next"></span>',
            customPaging: function(slick,index) {
                return '<a>' + (index + 1) + '</a>';
            }
        })

    }

    const projectsSlider = $('#slider__container_projects');

    projectsSlider.slick({
        dots: true,
        appendArrows: $('#project__slider_actions'),
        appendDots: $('#project__slider_pagination'),
        prevArrow: '<span class="slider__arrow prev"></span>',
        nextArrow: '<span class="slider__arrow next"></span>',
        customPaging: function(slick,index) {
            return '<a>' + (index + 1) + '</a>';
        }
    })

})